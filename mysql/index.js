require('dotenv').config();
const mysql = require('mysql');

class Mysql {
    constructor() {
        this.connection = mysql.createConnection({
            host: process.env.D_MYSQL_HOST,
            user: process.env.D_MYSQL_USERNAME,
            password: process.env.D_MYSQL_PASSWORD,
            database: process.env.D_MYSQL_DB
        });

        return this.connection.connect();
    }

    async getUserSettings() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'SELECT ' +
                        '`us`.`usr_username`,' +
                        '`us`.`id` as `user_id`,' +
                        '`us`.`usr_client_id`,' +
                        '`us`.`usr_client_id_test`,' +
                        '`us`.`usr_client_secret`,' +
                        '`us`.`usr_client_secret_test`,' +
                        '`us`.`usr_type_instrument`,' +
                        '`us`.`usr_rolling_status`,' +
                        '`us`.`usr_freeze_button`,' +
                        '`us`.`usr_hedge_or_stoploss`,' +
                        '`dsb`.*,' +
                        '`dse`.*,' +
                        '`ti`.`ti_name`, ' +
                        '`stp`.`stp_step`, ' +
                        '`rb`.*,' +
                        '`re`.*' +
                     'FROM ' +
                        '`god_db9`.`users` AS `us`' +
                        'LEFT JOIN `god_db9`.`der_setting_btc` AS `dsb` ON (`dsb`.`btc_id_user` = `us`.`id` AND `dsb`.`btc_type_instrument` = `us`.`usr_type_instrument`)' +
                        'LEFT JOIN `god_db9`.`der_setting_eth` AS `dse` ON (`dse`.`eth_id_user` = `us`.`id` AND `dse`.`eth_type_instrument` = `us`.`usr_type_instrument`)' +
                        'LEFT JOIN `god_db9`.`rolling_btc` AS `rb` ON (`rb`.`btc_id_user_roll` = `us`.`id` AND `rb`.`btc_type_instrument_roll` = `us`.`usr_type_instrument`)' +
                        'LEFT JOIN `god_db9`.`rolling_eth` AS `re` ON (`re`.`eth_id_user_roll` = `us`.`id` AND `re`.`eth_type_instrument_roll` = `us`.`usr_type_instrument`)' +
                        'LEFT JOIN `god_db9`.`type_instrument` AS `ti` ON `ti`.`id` = `us`.`usr_type_instrument`' +
                        'LEFT JOIN `god_db9`.`stoploss_settings` AS `stp` ON `stp`.`stp_id_user` = `us`.`id`' +
                     'WHERE (' +
                         '`us`.`usr_active_status` = ? AND ' +
                         '`us`.`usr_blacklist_status` = ? AND ' +
                         '`us`.`usr_sub_status` = ? AND ' +
                         '`ti`.`ti_name` = ? AND ( ' +
                         '`us`.`usr_client_id` IS NOT NULL AND' +
                         '`us`.`usr_client_id` <> ? AND ' +
                         '`us`.`usr_client_secret` IS NOT NULL AND' +
                         '`us`.`usr_client_secret` <> ? AND' +
                         '`us`.`id` = ? ) OR ( ' +
                         '`us`.`usr_client_id_test` IS NOT NULL AND' +
                         '`us`.`usr_client_id_test` <> ? AND ' +
                         '`us`.`usr_client_secret_test` IS NOT NULL AND' +
                         '`us`.`usr_client_secret_test` <> ?  AND' +
                         '`us`.`id` = ? )' +
                     ') LIMIT 1;',
                timeout: 40000, // 40s
                values: ['1', '0', '1', process.env.D_INSTRUMENT_NAME.toLowerCase(), '', '', process.env.D_USER_ID, '', '', process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async setUserFreezeButton(status1, status2) {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'UPDATE ' +
                        '`god_db9`.`users` ' +
                     'SET ' +
                        '`users`.`usr_rolling_status` = ? ' +
                     'WHERE (' +
                         '`users`.`id` = ?' +
                     ') LIMIT 1;'
                ,
                timeout: 40000,
                values: [status1, process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async getGrids() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'SELECT ' +
                    '`us`.`usr_username`,' +
                    '`us`.`id` as `user_id`,' +
                    '`us`.`usr_client_id`,' +
                    '`us`.`usr_client_secret`,' +
                    '`us`.`usr_type_instrument`,' +
                    '`us`.`usr_rolling_status`,' +
                    '`us`.`usr_freeze_button`,' +
                    '`dsb`.*,' +
                    '`dse`.*,' +
                    '`ti`.`ti_name`, ' +
                    '`stp`.`stp_step`, ' +
                    '`rb`.*,' +
                    '`re`.*' +
                    'FROM ' +
                    '`god_db9`.`users` AS `us`' +
                    'LEFT JOIN `god_db9`.`der_setting_btc` AS `dsb` ON (`dsb`.`btc_id_user` = `us`.`id` AND `dsb`.`btc_type_instrument` = `us`.`usr_type_instrument`)' +
                    'LEFT JOIN `god_db9`.`der_setting_eth` AS `dse` ON (`dse`.`eth_id_user` = `us`.`id` AND `dse`.`eth_type_instrument` = `us`.`usr_type_instrument`)' +
                    'LEFT JOIN `god_db9`.`rolling_btc` AS `rb` ON (`rb`.`btc_id_user_roll` = `us`.`id` AND `rb`.`btc_type_instrument_roll` = `us`.`usr_type_instrument`)' +
                    'LEFT JOIN `god_db9`.`rolling_eth` AS `re` ON (`re`.`eth_id_user_roll` = `us`.`id` AND `re`.`eth_type_instrument_roll` = `us`.`usr_type_instrument`)' +
                    'LEFT JOIN `god_db9`.`type_instrument` AS `ti` ON `ti`.`id` = `us`.`usr_type_instrument`' +
                    'LEFT JOIN `god_db9`.`stoploss_settings` AS `stp` ON `stp`.`stp_id_user` = `us`.`id`' +
                    'WHERE (' +
                    '`ti`.`ti_name` = ? AND' +
                    '`us`.`id` = ? ' +
                    ')',
                timeout: 40000, // 40s
                values: [process.env.D_INSTRUMENT_NAME.toLowerCase(), process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async getUpdateContainer() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'SELECT ' +
                    '`act`.*' +
                    'FROM ' +
                    '`god_db9`.`actions` AS `act`' +
                    'WHERE (' +
                    '`act`.`act_id_user` = ? ' +
                    ') LIMIT 1; ',
                timeout: 40000, // 40s
                values: [process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    /*
    async getHedgeStoploss() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'SELECT ' +
                    '`us`.`usr_hedge_or_stoploss`' +
                    'FROM ' +
                    '`god_db9`.`users` AS `us`' +
                    'WHERE (' +
                    '`us`.`id` = ? ' +
                    ') LIMIT 1; ',
                timeout: 40000, // 40s
                values: [process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results[0].usr_hedge_or_stoploss);
            });
        });
    }
    */

    async getIpContainer() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'SELECT ' +
                    '`con`.`cnt_ip`' +
                    'FROM ' +
                    '`god_db9`.`containers` AS `con`' +
                    'WHERE (' +
                    '`con`.`cnt_id_user` = ? ' +
                    ') LIMIT 1; ',
                timeout: 40000, // 40s
                values: [process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                // console.log(results[0].cnt_ip);
                resolve(results[0].cnt_ip);
            });
        });
    }

    async setUpdateContainer() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'UPDATE ' +
                    '`god_db9`.`actions`' +
                    'SET ' +
                    '`actions`.`act_docker_update` = ? ' +
                    'WHERE (' +
                    '`actions`.`act_id_user` = ?' +
                    ') LIMIT 1; '
                ,
                timeout: 40000,
                values: [0, process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async setUpdateFreezeStatus() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'UPDATE ' +
                    '`god_db9`.`users`' +
                    'SET ' +
                    '`users`.`usr_rolling_status` = ? ' +
                    'WHERE (' +
                    '`users`.`id` = ?' +
                    ') LIMIT 1; '
                ,
                timeout: 40000,
                values: [1, process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async setPauseRobot(state) {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'UPDATE ' +
                    '`god_db9`.`actions`' +
                    'SET ' +
                    '`actions`.`act_robot_pause` = ? ' +
                    'WHERE (' +
                    '`actions`.`act_id_user` = ?' +
                    ') LIMIT 1; '
                ,
                timeout: 40000,
                values: [state, process.env.D_USER_ID] // 1
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async setStoplossTimerActive(state) {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'UPDATE ' +
                    '`god_db9`.`stoploss_settings`' +
                    'SET ' +
                    '`stoploss_settings`.`stp_timer_active` = ?,  ' +
                    '`stoploss_settings`.`stp_date` = NOW() ' +
                    'WHERE (' +
                    '`stoploss_settings`.`stp_id_user` = ?' +
                    ') LIMIT 1; '
                ,
                timeout: 40000,
                values: [state, process.env.D_USER_ID] // 1
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async setTypeOfEmail(type) {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'UPDATE ' +
                    '`god_db9`.`actions`' +
                    'SET ' +
                    '`actions`.`act_send_email` = ? ' +
                    'WHERE (' +
                    '`actions`.`act_id_user` = ?' +
                    ') LIMIT 1; '
                ,
                timeout: 40000,
                values: [type, process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async updateHedgeProfitGlobalBtc() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'UPDATE ' +
                    '`god_db9`.`der_setting_btc`' +
                    'SET ' +
                    '`der_setting_btc`.`btc_hedge_profit_global` = ? ' +
                    'WHERE (' +
                    '`der_setting_btc`.`btc_id_user` = ?' +
                    ') LIMIT 1; '
                ,
                timeout: 40000,
                values: [0, process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async updateHedgeProfitGlobalEth() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'UPDATE ' +
                    '`god_db9`.`der_setting_eth`' +
                    'SET ' +
                    '`der_setting_eth`.`eth_hedge_profit_global` = ? ' +
                    'WHERE (' +
                    '`der_setting_eth`.`eth_id_user` = ?' +
                    ') LIMIT 1; '
                ,
                timeout: 40000,
                values: [0, process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async getRedisSettings() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'SELECT ' +
                    '`rds`.*' +
                    'FROM ' +
                    '`god_db9`.`redis` AS `rds`' +
                    'WHERE (' +
                    '`rds`.`id` = ? ' +
                    ') LIMIT 1; ',
                timeout: 40000, // 40s
                values: [1]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                // console.log(results[0].cnt_ip);
                resolve(results);
            });
        });
    }

    async getHedgeStoploss() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'SELECT ' +
                    '`us`.`usr_hedge_or_stoploss`' +
                    'FROM ' +
                    '`god_db9`.`users` AS `us`' +
                    'WHERE (' +
                    '`us`.`id` = ? ' +
                    ') LIMIT 1; ',
                timeout: 40000, // 40s
                values: [process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async getStepStoploss() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'SELECT ' +
                    '`stp`.`stp_step`' +
                    'FROM ' +
                    '`god_db9`.`stoploss_settings` AS `stp`' +
                    'WHERE (' +
                    '`stp`.`stp_id_user` = ? ' +
                    ') LIMIT 1; ',
                timeout: 40000, // 40s
                values: [process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }

    async doneMysql() {
        return new Promise((resolve, reject) => {
            this.connection.end((err) => {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                resolve(null);
            });
        });
    }
}

module.exports = Mysql;