require('dotenv').config();
const fetch = require('node-fetch');
const Mysql = require('../mysql');

class Api {

    constructor(...params) {

        this.clientId = params[0];
        this.clientSecret = params[1];
        this.redis = params[2];
        this.counterForExpiresIn = params[3];
        this.counterForExpiresInRe = params[3];
        this.user = params[4];
        this.domain = params[5];

        this.accessToken = '';
        this.expiresIn = '';

        this.headers = {};

        this.time = 10;

        this.lastPrice = 0;
        this.activePosition = {};
        this.activePositionOldSize = 0;
        this.directionOld = '';
        this.hedgeActivePosition = {};

        this.hedgeBuyObj = {};
        this.hedgeSellObj = {};

        this.gridBuy = [];
        this.gridSell = [];

        this.gridBuyRoll = [];
        this.gridSellRoll = [];

        this.amount = 0;
        this.amountBuyDirection = 0;
        this.amountSellDirection = 0;

        this.amountBuyStopDirection = 0;
        this.amountSellStopDirection = 0;

        this.gridStep = params[6]; // process.env.D_GRID_STEP; // 10;
        this.gridOrders = params[7]; // process.env.D_GRID_ORDERS; // 15;
        this.gridOrderSize = params[8]; // process.env.D_GRID_ORDER_SIZE; // 50;

        this.hedgeOffset = params[9]; // process.env.D_HEDGE_OFFSET; // 50;
        this.userId = params[10];

        this.stepMultiplier = params[11];
        this.sizeMultiplier = params[12];
        this.hedgeOrStoploss = params[13];

        this.gridStepRoll = params[14];
        this.gridOrdersRoll = params[15];
        this.gridOrderSizeRoll = params[17];

        this.stoplossStep = params[16];

        this.hedgeBuyCounter = 0;
        this.hedgeSellCounter = 0;

        this.checkSizes = [];
        this.checkPrices = [];

        this.middlePriceBuy = 0;
        this.countSizeBuy = 0;
        this.middlePriceSell = 0;
        this.countSizeSell = 0;

        this.lastBuyOrderPrice = 0;
        this.lastSellOrderPrice = 0;

        this.firstZeroMoving = 0;
        this.flagGetActions = 0;

        this.flagActRobotPause = 0;

        this.openOrdersLimitBuyIds = [];
        this.openOrdersLimitSellIds = [];

        this.openOrdersStoplossBuyIds = [];
        this.openOrdersStoplossSellIds = [];

        this.openOrdersBuys = [];
        this.openOrdersSells = [];

        this.lastBuyOrderId = '';
        this.lastSellOrderId = '';

        this.globalStopFlag = 0;

        this.flagPauseWhenDerebitDisRedis = 0;

        this.userTradesBuys = [];
        this.userTradesSells = [];

        return this;
    }

    async auth() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'public/auth';
            const params = `?client_id=${this.clientId}&client_secret=${this.clientSecret}&grant_type=client_credentials&scope=${this.user}`;
            const res = await fetch(`${url}${method}${params}`);
            const body = await res.json();

            if (typeof body.error !== "undefined") {
                this.accessToken = '';
                console.error('accessToken is empty:', body.error);
            }

            if (typeof body.result !== "undefined") {
                this.accessToken = body.result.access_token;
                this.expiresIn = body.result.expires_in;

                this.headers = {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${this.accessToken}`
                };
                console.log(this.accessToken);
            }
        } catch (e) {
            console.error('auth() error:', e.message);
        }
        return false;
    };

    async getTicker() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'public/ticker';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL`;
            const res = await fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });
            const body = await res.json();

            if (typeof body.error !== "undefined") {
                console.log(body.error);
            }

            this.lastPrice = parseFloat(body.result.last_price);
        } catch (e) {
            console.error('getTicker() error:', e.message);
        }

        return false;
    };

    async getOrdersHistory() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/get_user_trades_by_instrument';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&count=40&include_old=true`;
            const res = await fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            this.userTradesBuys = [];
            this.userTradesSells = [];
            const body = await res.json();
            body.result.trades.forEach((dataset) => {
                // console.log(dataset);
                const obj = {};
                obj.Size = dataset.amount;
                obj.Price = dataset.price;
                obj.State = dataset.state;
                obj.Id = dataset.order_id;

                if (dataset.state === 'filled' && dataset.direction === 'buy') {
                    this.userTradesBuys.push(obj);
                }

                if (dataset.state === 'filled' && dataset.direction === 'sell') {
                    this.userTradesSells.push(obj);
                }
                // console.log(dataset.state, dataset.direction, dataset.amount);
            })


        } catch (e) {
            console.error('getOrdersHistory() error #2:', e.message, 'accessToken:', this.accessToken);
            this.accessToken = '';
        }
        return false;
    };

    async getPosition() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/get_position';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL`;
            const res = await fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            this.activePosition = await res.json();

            if (typeof this.activePosition.error !== "undefined") {
                console.log('getPosition() error #1:', this.activePosition.error);
                if (this.activePosition.error.code === 13004) {
                    this.accessToken = '';
                }
            }

        } catch (e) {
            console.error('getPosition() error #2:', e.message, 'accessToken:', this.accessToken);
            this.accessToken = '';
        }
        return false;
    };

    async getHedgePosition() {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/get_position';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-27SEP19`;
            const res = await fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const body = await res.json();

            if (typeof body.error !== "undefined") {
                console.log('getHedgePosition() error #1:', this.body);
                // if (this.resJson.error.code === 13004) {
                    // this.accessToken = '';
                // }
            }

            if (typeof body.result !== "undefined") {
                this.hedgeActivePosition = Object.assign({}, body);
            }
        } catch (e) {
            console.error('getHedgePosition() error #2:', e.message, 'accessToken:', this.accessToken);
            this.accessToken = '';
        }

        return false;
    };

    async cancelOrders(type = 'all') {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/cancel_all_by_currency';
        const params = `?currency=${process.env.D_INSTRUMENT_NAME}&type=${type}`;
        const res = await fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });
        await res.json();

        return false;
    };

    async cancelOrderOne(orderId) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/cancel';
        const params = `?order_id=${orderId}`;
        const res = await fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });
        await res.json();

        return false;
    };

    async setBuyLimit(orderSize, orderPrice) {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/buy';
            const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=limit&price=${orderPrice}&post_only=true`;
            const res = await fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });
            const result = await res.json();

            if (typeof result.error !== "undefined") {
                console.log('setBuyLimit() error #1:', result.error);
            }

            // console.log(result);
        } catch (e) {
            console.error('setBuyLimit() error #2:', e.message);
        }

        return false;
    };

    async setBuyLimitOne(orderSize, orderPrice) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/buy';
        const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=limit&price=${orderPrice}&post_only=true`;
        const res = await fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.lastBuyOrderId = '';
        const res1 = await res.json();
        this.lastBuyOrderId = res1.result.order.order_id;

        return false;
    };

    async setBuyStopLoss(orderSize, orderPrice) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/buy';
        const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=stop_market&trigger=last_price&stop_price=${orderPrice}&reduce_only=true`;
        const res = await fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });
        // await res.json();

        return false;
    };

    async setSellLimit(orderSize, orderPrice) {
        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/sell';
            const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=limit&price=${orderPrice}&post_only=true`;
            const res = await fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const result = await res.json();

            if (typeof result.error !== "undefined") {
                console.log('setSellLimit() error #1:', result.error);
            }

            // console.log(result);
        } catch (e) {
            console.error('setSellLimit() error #2:', e.message);
        }

        return false;
    };

    async setSellLimitOne(orderSize, orderPrice) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/sell';
        const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=limit&price=${orderPrice}&post_only=true`;
        const res = await fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.lastSellOrderId = '';
        const res1 = await res.json();
        this.lastSellOrderId = res1.result.order.order_id;

        return false;
    };

    async setSellStopLoss(orderSize, orderPrice) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/sell';
        const params = `?amount=${orderSize}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=stop_market&trigger=last_price&stop_price=${orderPrice}&reduce_only=true`;
        const res = await fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });
        // await res.json();

        return false;
    };

    async hedgeBuy(size) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/buy';
        const params = `?amount=${size}&instrument_name=${process.env.D_INSTRUMENT_NAME}-27SEP19&type=market`;
        const res = await fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.hedgeBuyObj = await res.json();

        return false;
    };

    async hedgeSell(size) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/sell';
        const params = `?amount=${size}&instrument_name=${process.env.D_INSTRUMENT_NAME}-27SEP19&type=market`;
        const res = await fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.hedgeSellObj = await res.json();

        return false;
    };

    async marketPerpetaulBuy(size) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/buy';
        const params = `?amount=${size}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=market`;
        const res = await fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.hedgeBuyObj = await res.json();

        return false;
    }

    async marketPerpetaulSell(size) {
        const url = `https://${this.domain}/api/v2/`;
        const method = 'private/sell';
        const params = `?amount=${size}&instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL&type=market`;
        const res = await fetch(`${url}${method}${params}`, {
            headers: this.headers,
        });

        this.hedgeSellObj = await res.json();

        return false;
    };

    async getOpenOrders() {
        this.amountBuyDirection = 0;
        this.amountSellDirection = 0;

        this.amountBuyStopDirection = 0;
        this.amountSellStopDirection = 0;

        this.openOrdersLimitBuyIds = [];
        this.openOrdersLimitSellIds = [];

        this.openOrdersStoplossBuyIds = [];
        this.openOrdersStoplossSellIds = [];

        this.openOrdersBuys = [];
        this.openOrdersSells = [];

        try {
            const url = `https://${this.domain}/api/v2/`;
            const method = 'private/get_open_orders_by_instrument';
            const params = `?instrument_name=${process.env.D_INSTRUMENT_NAME}-PERPETUAL`;
            const res = await fetch(`${url}${method}${params}`, {
                headers: this.headers,
            });

            const len = await res.json();

            if (typeof len.error !== "undefined") {
                console.log('getOrders() error #1:', len.error);
            }

            if (typeof len.result !== "undefined") {
                if (len.result.length !== 0) {
                    len.result.forEach((dataset) => {
                        if (dataset.direction === 'buy' && dataset.order_type === 'limit') {
                            this.amountBuyDirection += 1;
                            this.openOrdersLimitBuyIds.push(dataset.order_id);
                            const obj = {};
                            obj.Size = dataset.amount;
                            obj.Price = dataset.price;
                            obj.Id = dataset.order_id;
                            this.openOrdersBuys.push(obj);
                        }
                        if (dataset.direction === 'sell' && dataset.order_type === 'limit') {
                            this.amountSellDirection += 1;
                            this.openOrdersLimitSellIds.push(dataset.order_id);
                            const obj = {};
                            obj.Size = dataset.amount;
                            obj.Price = dataset.price;
                            obj.Id = dataset.order_id;
                            this.openOrdersSells.push(obj);
                        }

                        if (dataset.direction === 'buy' && dataset.order_type === 'stop_market') {
                            this.amountBuyStopDirection += 1;
                            this.openOrdersStoplossBuyIds.push(dataset.order_id);
                        }
                        if (dataset.direction === 'sell' && dataset.order_type === 'stop_market') {
                            this.amountSellStopDirection += 1;
                            this.openOrdersStoplossSellIds.push(dataset.order_id);
                        }
                    });

                    this.openOrdersBuys.sort(function(a, b) {
                        return b.Price - a.Price;
                    });

                    this.openOrdersSells.sort(function(a, b) {
                        return a.Price - b.Price;
                    });
                }
            }

            // For Test. Need make .env dev
            // console.log('Buy', this.amountBuyDirection);
            // console.log('Sell', this.amountSellDirection);
            this.amount = len.result.length;
        } catch (e) {
            console.error('getOrders() error #2:', e.message);
        }
        return false;
    };

    async checkAuth(type) {
        if (type === 'just-check') {
            this.counterForExpiresInRe = 0;
            this.counterForExpiresIn += this.time;
        }

        if (this.counterForExpiresIn >= 800) {
            this.counterForExpiresIn = 0;
            await this.auth();
        }

        if (type === 're-auth') {
            this.counterForExpiresIn = 0;
            this.counterForExpiresInRe += this.time * 0.5;
        }

        if (this.counterForExpiresInRe >= 800) {
            this.counterForExpiresInRe = 0;
            await this.auth();
        }

        if (type === 'redis-check') {
            this.counterForExpiresIn = 0;
            this.counterForExpiresInRe = 0;
            await this.auth();
        }

        return false;
    };

    async restartContainer(ip) {
        const body = { t: 1 };

        const headersData = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        const url = `http://${ip}:4243/containers/`;
        const params = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}/`;
        const method = 'restart';
        const res = await fetch(`${url}${params}${method}`, {
            method: 'post',
            body: 't=2',
            headers: headersData,
        });
        await res.text();
        console.log('Container was restarted!', res.status);

        return false;
    };

    async stopContainer(ip) {
        const body = { t: 1 };

        const headersData = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        const url = `http://${ip}:4243/containers/`;
        const params = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}/`;
        const method = 'stop';
        const res = await fetch(`${url}${params}${method}`, {
            method: 'post',
            body: 't=2',
            headers: headersData,
        });
        await res.text();
        console.log('Container was stopped!', res.status);

        return false;
    };

    async restartContainerNew(ip) {
        const headersData = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };

        const url = `http://${ip}:4243/containers/`;
        const params = `${process.env.D_USER_ID}_${process.env.D_INSTRUMENT_NAME.toLowerCase()}/`;
        const method = 'restart';
        const res = await fetch(`${url}${params}${method}`, {
            method: 'post',
            body: 't=2',
            headers: headersData,
        });
        console.log(res.status);
        await res.text();

        return false;
    };

    async setGrids() {
        // await this.getOpenOrders();
        if (this.openOrdersBuys.length === 0 || this.openOrdersSells.length === 0) {
            await this.cancelOrders();
            this.flagGetActions = 1;
            await this.setSettings();
            this.gridBuy = [];

            let gridStepBuy = this.gridStepRoll;
            let gridStepSell = this.gridStepRoll;
            let gridOrderSizeBuy = this.gridOrderSizeRoll;
            let gridOrderSizeSell = this.gridOrderSizeRoll;

            let objBuy = {};
            objBuy.Price = this.lastPrice - gridStepBuy;
            objBuy.Size = gridOrderSizeBuy;
            this.gridBuy.push(objBuy);
            this.checkSizes = [];
            this.middlePriceBuy = 0;
            this.countSizeBuy = 0;
            this.lastBuyOrderPrice = 0;
            this.checkSizes.push(objBuy.Size);
            this.middlePriceBuy = objBuy.Size * objBuy.Price;
            this.countSizeBuy = objBuy.Size;

            for (let i = 0; i <= this.gridOrdersRoll - 2; i++) {
                let obj = {};
                obj.Price = this.gridBuy[i].Price - gridStepBuy;
                obj.Size = gridOrderSizeBuy;
                this.gridBuy.push(obj);
                this.checkSizes.push(this.checkSizes[i] + obj.Size);
                this.middlePriceBuy += obj.Size * obj.Price;
                this.countSizeBuy += obj.Size;
            }

            this.lastBuyOrderPrice = this.gridBuy[this.gridBuy.length - 1].Price - this.stoplossStep;

            console.log('gridBuy[]');
            console.log(this.gridBuy);

            this.gridSell = [];
            this.middlePriceSell = 0;
            this.countSizeSell = 0;
            this.lastSellOrderPrice = 0;
            let objSell = {};
            objSell.Price = this.lastPrice + gridStepSell;
            objSell.Size = gridOrderSizeSell;
            this.gridSell.push(objSell);
            this.middlePriceSell = objSell.Size * objSell.Price;
            this.countSizeSell = objSell.Size;

            for (let i = 0; i <= this.gridOrdersRoll - 2; i++) {
                let obj = {};
                obj.Price = this.gridSell[i].Price + gridStepSell;
                obj.Size = gridOrderSizeSell;
                this.gridSell.push(obj);
                this.middlePriceSell += obj.Size * obj.Price;
                this.countSizeSell += obj.Size;
            }

            this.lastSellOrderPrice = this.gridSell[this.gridSell.length - 1].Price + this.stoplossStep;

            console.log('gridSell[]');
            console.log(this.gridSell);

            await this.setOrders();
        }

        if (this.openOrdersBuys.length !== 0 && this.openOrdersSells.length !== 0) {
            this.flagGetActions = 0;
            // BUY ORDER
            if (
                this.openOrdersBuys.length > 0 && this.openOrdersSells.length === this.gridOrdersRoll
            ) {
                let gridStepRollSell = this.gridStepRoll;
                let gridOrderSizeRollSell = this.gridOrderSizeRoll;

                this.gridBuy = [];
                this.openOrdersBuys.forEach((dataset) => {
                    this.gridBuy.unshift(dataset);
                });
                const pos = this.gridOrdersRoll - this.gridBuy.length;

                this.gridBuyRoll = [];
                for (let i = 0; i <= pos - 1; i++) {
                    let obj = {};
                    obj.Size = gridOrderSizeRollSell;
                    obj.Price = this.openOrdersBuys.length !== 0 ? this.openOrdersBuys[this.openOrdersBuys.length - 1].Price - ((i + 1) * gridStepRollSell) : this.activePosition.result.average_price - (i + 1) * gridStepRollSell;
                    this.gridBuyRoll.push(obj);
                }

                this.gridSellRoll = [];
                // await this.getOrdersHistory();
                for (let i = 0; i <= pos - 1; i++) {
                    let obj = {};
                    obj.Price = this.openOrdersSells[0].Price - ((i + 1) * gridStepRollSell);
                    obj.Size = gridOrderSizeRollSell;
                    this.gridSellRoll.push(obj);
                }

                console.log('BUYS:');
                console.log('pos:', pos, 'gridStepRollSell:', gridStepRollSell, 'gridOrderSizeRollSell:', gridOrderSizeRollSell);

                //console.log('gridBuy[]:');
                //console.log(this.gridBuy);
                console.log('gridBuyRoll[]:');
                console.log(this.gridBuyRoll);
                console.log('openOrdersBuys[]:');
                console.log(this.openOrdersBuys);
                console.log(' ------------------- ');
                //console.log('gridSell[]:');
                //console.log(this.gridSell);
                console.log('gridSellRoll[]:');
                console.log(this.gridSellRoll);
                console.log('openOrdersSells[]:');
                console.log(this.openOrdersSells);
                // console.log('BUYS #3 gridSellRoll:', pos, this.gridSellRoll, this.openOrdersSells[this.openOrdersSells.length - 1].Price);
            }

            // SELL ORDER
            if (
                this.openOrdersSells.length > 0 && this.openOrdersBuys.length === this.gridOrdersRoll
            ) {
                let gridStepRollBuy = this.gridStepRoll;
                let gridOrderSizeRollBuy = this.gridOrderSizeRoll;

                this.gridSell = [];
                this.openOrdersSells.forEach((dataset) => {
                    this.gridSell.push(dataset);
                });
                const pos = this.gridOrdersRoll - this.gridSell.length;

                this.gridSellRoll = [];
                for (let i = 0; i <= pos - 1; i++) {
                    let obj = {};
                    obj.Size = gridOrderSizeRollBuy;
                    obj.Price = this.openOrdersSells.length !== 0 ? this.openOrdersSells[this.openOrdersSells.length - 1].Price + ((i + 1) * gridStepRollBuy) : this.activePosition.result.average_price - (i + 1) * gridStepRollSell;
                    this.gridSellRoll.push(obj);
                }
                // console.log('SELLS #2 gridSell:', pos, this.gridSell, this.gridSellRoll);

                this.gridBuyRoll = [];
                // await this.getOrdersHistory();
                for (let i = 0; i <= pos - 1; i++) {
                    let obj = {};
                    obj.Price = this.openOrdersBuys[0].Price + (i + 1) * gridStepRollBuy;
                    obj.Size = gridOrderSizeRollBuy;
                    this.gridBuyRoll.push(obj);
                }
                // console.log('SELLS #3 gridBuyRoll:', pos, this.gridBuyRoll, this.openOrdersBuys[this.openOrdersBuys.length - 1].Price);
                console.log('SELLS:');
                console.log('pos:', pos, 'gridStepRollBuy:', gridStepRollBuy, 'gridOrderSizeRollBuy:', gridOrderSizeRollBuy);

                //console.log('gridBuy[]:');
                //console.log(this.gridBuy);
                console.log('gridBuyRoll[]:');
                console.log(this.gridBuyRoll);
                console.log('openOrdersBuys[]:');
                console.log(this.openOrdersBuys);
                console.log(' ------------------- ');
                //console.log('gridSell[]:');
                //console.log(this.gridSell);
                console.log('gridSellRoll[]:');
                console.log(this.gridSellRoll);
                console.log('openOrdersSells[]:');
                console.log(this.openOrdersSells);
            }

            if (
                this.openOrdersSells.length !== this.gridOrdersRoll && this.openOrdersBuys.length !== this.gridOrdersRoll
            ) {
                await this.cancelOrders();
                await this.setGrids();
            }

            await this.setOrders();
        }

        return false;
    };

    async getRedis() {
        return new Promise((resolve, reject) => {
          this.redis.pipeline([
            ['get', `${this.user}:buys`],
            ['get', `${this.user}:sells`],
          ]).exec((err, results) => {
            if (err) reject(err);

            resolve(results);
          });
        });
    };

    async delRedis() {
        return new Promise((resolve, reject) => {
          this.redis.pipeline([
            ['del', `${this.user}:buys`],
            ['del', `${this.user}:sells`],
          ]).exec((err, results) => {
            if (err) reject(err);

            console.log('Del result:', results);

            resolve(results);
          });
        });
    };

    async setRedis() {
        return new Promise((resolve, reject) => {
          this.redis.pipeline([
            ['set', `${this.user}:buys`, JSON.stringify(this.gridBuy)],
            ['set', `${this.user}:sells`, JSON.stringify(this.gridSell)],
          ]).exec((err, results) => {
            if (err) reject(err);

            resolve(results);
          });
        });
    };

    async setOrders() {
        // await this.getOpenOrders();
        // console.log(this.hedgeOrStoploss, this.flagActRobotPause, this.activePosition.result.direction);
        // SET LIMIT ORDERS
        // ================== HOME ==================
        // FIRST CONDITION IS HEDGE; SECOND CONDITIONS IS STOPLOSS + PAUSE + NO ZERO

        if (this.openOrdersSells.length === 0 || this.openOrdersBuys.length === 0) {
            let arr1 = [];
            // console.log('gridBuy[], #2');
            // console.log(this.gridBuy);
            for (let objBuy of this.gridBuy) {
                // await this.setBuyLimit(objBuy.Size, objBuy.Price);
                arr1.push(this.setBuyLimit(objBuy.Size, objBuy.Price));
                if (arr1.length === 4) {
                    await Promise.all(arr1);
                    arr1 = [];
                }
            }
            if (arr1.length !== 0) {
                await Promise.all(arr1);
            }

            // console.log('gridSell[], #2');
            // console.log(this.gridSell);
            let arr2 = [];
            for (let objSell of this.gridSell) {
                // await this.setSellLimit(objSell.Size, objSell.Price);
                arr2.push(this.setSellLimit(objSell.Size, objSell.Price));
                if (arr2.length === 4) {
                    await Promise.all(arr2);
                    arr2 = [];
                }
            }
            if (arr2.length !== 0) {
                await Promise.all(arr2);
            }

            await this.getOpenOrders();
            console.log('openOrdersBuys[] .. #2:');
            console.log(this.openOrdersBuys);
            console.log(' ------------------- ');
            console.log('openOrdersSells[] .. #2:');
            console.log(this.openOrdersSells);
        }

        if (this.openOrdersBuys.length !== 0 && this.openOrdersSells.length !== 0) {
            // SELL
            if (this.openOrdersSells.length !== this.gridOrdersRoll && this.openOrdersBuys.length === this.gridOrdersRoll) {
                const arr1 = [];
                const pos = this.gridOrdersRoll - this.openOrdersSells.length;
                let index = this.gridOrdersRoll;
                for (let objBuyO of this.openOrdersBuys) {
                    if (index <= pos) {
                        console.log(index, pos, objBuyO.Id);
                        await this.cancelOrderOne(objBuyO.Id);
                    }
                    index -= 1;
                }

                if (this.gridSellRoll.length !== 0) {
                    for (let objSellRoll of this.gridSellRoll) {
                        await this.setSellLimit(objSellRoll.Size, objSellRoll.Price);
                    }
                }

                if (this.gridBuyRoll.length !== 0) {
                    for (let objBuyRoll of this.gridBuyRoll) {
                        await this.setBuyLimit(objBuyRoll.Size, objBuyRoll.Price);
                    }
                }
                // await Promise.all([arr1]);
            }

            // BUY
            if (this.openOrdersSells.length === this.gridOrdersRoll && this.openOrdersBuys.length !== this.gridOrdersRoll) {
                const arr2 = [];
                const pos = this.gridOrdersRoll - this.openOrdersBuys.length;
                let index = this.gridOrdersRoll;
                for (let objSellO of this.openOrdersSells) {
                    if (index <= pos) {
                        await this.cancelOrderOne(objSellO.Id);
                    }
                    index -= 1;
                }

                if (this.gridBuyRoll.length !== 0) {
                    for (let objBuyRoll of this.gridBuyRoll) {
                        await this.setBuyLimit(objBuyRoll.Size, objBuyRoll.Price);
                    }
                }

                if (this.gridSellRoll.length !== 0) {
                    for (let objSellRoll of this.gridSellRoll) {
                        await this.setSellLimit(objSellRoll.Size, objSellRoll.Price);
                    }
                }
                // await Promise.all([arr2]);
            }
        }
        // ================== END ===================
        // SET LIMIT ORDERS
        return false;
    }

    async setSettings() {
        const mysql = new Mysql();
        const resultGR = await mysql.getGrids();
        // console.log(result);
        let gridStep = 0;
        let gridOrders = 0;
        let gridOrderSize = 0;
        let hedgeOffset = 0;
        let stepMultiplier = 1;
        let sizeMultiplier = 1;
        let hedgeOrStoploss = 0;
        let stoplossStep = 50;

        let gridStepRoll = 0;
        let gridOrdersRoll = 0;
        let gridOrderSizeRoll = 0;

        if (typeof resultGR !== "undefined") {
            Object.keys(resultGR).forEach(function (key) {
                stoplossStep = this[key]['stp_step'];

                const prefix = this[key]['ti_name'].toLowerCase();
                gridStep = this[key][`${prefix}_grid_step`];
                gridOrders = this[key][`${prefix}_grid_orders`];
                gridOrderSize = this[key][`${prefix}_grid_order_size`];
                hedgeOffset = this[key][`${prefix}_hedge_offset`];
                stepMultiplier = this[key][`${prefix}_step_multiplier`];
                sizeMultiplier = this[key][`${prefix}_size_multiplier`];

                gridStepRoll = this[key][`${prefix}_grid_step_roll`];
                gridOrdersRoll = this[key][`${prefix}_grid_orders_roll`];
                gridOrderSizeRoll = this[key][`${prefix}_grid_order_size_roll`];
            }, resultGR);
        }

        if (gridStep !== 0 && gridOrders !== 0 && gridOrderSize !== 0 && hedgeOffset !== 0) {
            this.gridStep = gridStep;
            this.gridOrders = gridOrders;
            this.gridOrderSize = gridOrderSize;
            this.hedgeOffset = hedgeOffset;
            this.stepMultiplier = stepMultiplier;
            this.sizeMultiplier = sizeMultiplier;

            this.stoplossStep = stoplossStep;

            this.gridStepRoll = gridStepRoll;
            this.gridOrdersRoll = gridOrdersRoll;
            this.gridOrderSizeRoll = gridOrderSizeRoll;
        } else {
            console.error("Something wrong. Grids are 0.");
        }

        const resultHS = await mysql.getHedgeStoploss();
        if (typeof resultHS !== "undefined") {
            Object.keys(resultHS).forEach(function (key) {
                hedgeOrStoploss = this[key]['usr_hedge_or_stoploss'];
            }, resultHS);
        }
        this.hedgeOrStoploss = hedgeOrStoploss;


        await mysql.doneMysql();
    }

    static async parseJsonAsyncFunc(jsonString) {
        return await JSON.parse(jsonString);
    }

    static isEmpty(obj) {
        for (let key in obj) {
            return false;
        }
        return true;
    }

    static isOdd(num) {
        return num % 2;
    }
}

module.exports = Api;