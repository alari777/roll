require('dotenv').config();
const Api = require('./api');
const Mysql = require('./mysql');
const Getuser = require('./getuser');
// const Redis = require('ioredis');

let globalStopFlagNative = 0;
let users = [];
let apis = [];

async function setApis() {
    for (let objUser of users) {
        if (objUser.robotStatus) {
            console.log(objUser.robotStatus);
            const apiAuth = new Api(
                objUser.clientId,
                objUser.clientSecret,
                objUser.redis,
                objUser.expiresIn,
                objUser.session,
                objUser.domain,
                objUser.gridStep,
                objUser.gridOrders,
                objUser.gridOrderSize,
                objUser.hedgeOffset,
                objUser.id,
                objUser.stepMultiplier,
                objUser.sizeMultiplier,
                objUser.hedgeOrStoploss,
                objUser.gridStepRoll,
                objUser.gridOrdersRoll,
                objUser.stoplossStep,
                objUser.gridOrderSizeRoll,
            );
            let obj = {};
            obj.Userid = objUser.id;
            obj.Obj = apiAuth;
            apis.push(obj);
        }
    }
}

const authTimer = setTimeout(async () => {
    console.log('Step #1: script starts.');
    users = await Getuser.getUsersFromMysql();
    console.log(users);

    await setApis();

    for (let api of apis) {
        if (apis.length !== 0) {
            console.log('Step #2: init datas:');
            await api.Obj.auth();
        }
    }

    const greeting = function delay() {
        setTimeout(async () => {
            // START
            // console.time('Total time');
            // console.log('Start ...');
            if (apis.length !== 0) {
                for (let api of apis) {
                    // await api.delRedis();
                    if (api.Obj.globalStopFlag === 0 && globalStopFlagNative === 0) {
                        if (api.Obj.accessToken !== '' && typeof api.Obj.accessToken !== "undefined") {
                            await api.Obj.checkAuth('just-check');

                            await api.Obj.getTicker();
                            await api.Obj.getPosition();
                            await api.Obj.getHedgePosition();
                            await api.Obj.getOpenOrders();

                            // const len1 = api.Obj.openOrdersSells.length;
                            // const len2 = api.Obj.openOrdersBuys.length;

                            const openSellsLen = api.Obj.openOrdersSells.length;
                            const openBuysLen = api.Obj.openOrdersBuys.length;
                            if (openSellsLen !== api.Obj.gridOrdersRoll || openBuysLen !== api.Obj.gridOrdersRoll) {
                                console.log('len open sell:', api.Obj.openOrdersSells.length);
                                console.log('len open buy:', api.Obj.openOrdersBuys.length);

                                if (openSellsLen !== api.Obj.gridOrdersRoll) console.log('Rolling SELL.');
                                if (openBuysLen !== api.Obj.gridOrdersRoll) console.log('Rolling BUY.');

                                await api.Obj.setGrids();
                                // await api.Obj.setOrders();
                            }
                        } else {
                            await api.Obj.checkAuth('re-auth');
                        }
                    } else {
                        console.log('Robot almost ready stop/restart ...', api.Obj.globalStopFlag, globalStopFlagNative);
                        clearTimeout(greeting);
                    }
                    greeting();
                }
            }
            // console.timeEnd('Total time');
        }, 1000);
    }

    greeting();

}, 500);