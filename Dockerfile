FROM node:carbon

# Install software 
RUN apt-get install -y git

RUN git config --global user.email "alari777@gmail.com"
RUN git config --global user.name "alari777"

RUN npm install forever -g
VOLUME /roll

RUN git clone -b master --single-branch https://bitbucket.org/alari777/roll.git /roll/

# Create app directory
WORKDIR /roll

EXPOSE 80
CMD git pull origin master && npm install -y && npm start