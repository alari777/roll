require('dotenv').config();
const fetch = require('node-fetch');
const Mysql = require('../mysql');

class Getuser {

    constructor(...params) {

    }

    static async getUsersFromMysql() {
        const mysql = new Mysql();
        const res = await mysql.getUserSettings();

        const usersInit = [];
        let counterForExpiresIn = 0;

        if (typeof res !== "undefined") {
            Object.keys(res).forEach(function (key) {
                let domain = 'deribit.com';
                const prefix = this[key]['ti_name'].toLowerCase();

                if (this[key][`${prefix}_domain`] === 0 || this[key][`${prefix}_domain`] === null) {
                    domain = 'deribit.com';
                } else {
                    domain = 'testapp.deribit.com';
                }

                const obj = {};
                obj.id = this[key]['user_id'];
                if (this[key][`${prefix}_domain`] === 0 || this[key][`${prefix}_domain`] === null) {
                    obj.clientId = this[key]['usr_client_id'];
                    obj.clientSecret = this[key]['usr_client_secret'];
                } else {
                    obj.clientId = this[key]['usr_client_id_test'];
                    obj.clientSecret = this[key]['usr_client_secret_test'];
                }
                obj.redis = {};
                obj.expiresIn = counterForExpiresIn;
                obj.session = `session:${this[key]['usr_username']}${this[key]['ti_name'].toUpperCase()}prod`;
                obj.domain = domain;
                obj.active = this[key]['usr_freeze_button'];
                obj.gridStep = this[key][`${prefix}_grid_step`];
                obj.gridOrders = this[key][`${prefix}_grid_orders`];
                obj.gridOrderSize = this[key][`${prefix}_grid_order_size`];
                obj.hedgeOffset = this[key][`${prefix}_hedge_offset`];
                obj.stepMultiplier = this[key][`${prefix}_step_multiplier`];
                obj.sizeMultiplier = this[key][`${prefix}_size_multiplier`];

                obj.gridStepRoll = this[key][`${prefix}_grid_step_roll`];
                obj.gridOrdersRoll = this[key][`${prefix}_grid_orders_roll`];
                obj.gridOrderSizeRoll = this[key][`${prefix}_grid_order_size_roll`];
                obj.gridTypeInstrumentRoll = this[key][`${prefix}_type_instrument_roll`];

                obj.robotStatus = this[key]['usr_rolling_status'];
                obj.hedgeOrStoploss = this[key]['usr_hedge_or_stoploss'];
                obj.stoplossStep = this[key]['stp_step'];

                usersInit.push(obj);
            }, res);
        }

        await mysql.doneMysql();
        return usersInit;
    }
}

module.exports = Getuser;
